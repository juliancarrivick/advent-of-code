module Day4 exposing (..)

import Browser
import Dict exposing (Dict)
import File exposing (File)
import File.Select as Select
import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)
import Parser exposing (..)
import Set exposing (Set)
import String
import Task



-- MAIN


main : Program () Model Msg
main =
    Browser.element { init = init, update = update, view = view, subscriptions = subscriptions }



-- MODEL


type alias Model =
    { output : Maybe Int }


init : () -> ( Model, Cmd Msg )
init _ =
    ( { output = Nothing }, Cmd.none )



-- UPDATE


type Msg
    = FileRequested
    | FileLoaded File
    | TextRead String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        FileRequested ->
            ( model, selectFile )

        FileLoaded file ->
            ( model, readTextContents file )

        TextRead content ->
            ( { model | output = Just (calculateFileValue content) }, Cmd.none )


selectFile : Cmd Msg
selectFile =
    Select.file [ "text/plain" ] FileLoaded


readTextContents : File -> Cmd Msg
readTextContents file =
    Task.perform TextRead (File.toString file)


calculateFileValue : String -> Int
calculateFileValue content =
    let
        cards =
            content
                |> String.lines
                |> List.map (run lineParser)
                |> List.filterMap Result.toMaybe
    in
    calcCardCopies cards
        |> Dict.values
        |> List.foldl (+) 0


calcCardCopies : List Card -> Dict Int Int
calcCardCopies cards =
    let
        cardWins =
            List.map (\c -> ( c.id, calculateObtainedCopies c )) cards

        initialCardCopies =
            Dict.fromList (List.map (\c -> ( c.id, 1 )) cards)
    in
    List.foldl (\next acc -> updateDictFromWins acc next) initialCardCopies cardWins


updateDictFromWins : Dict Int Int -> ( Int, List Int ) -> Dict Int Int
updateDictFromWins currWins newWins =
    let
        id =
            Tuple.first newWins

        copiesOfWinner =
            Dict.get id currWins
                |> Maybe.withDefault 1

        wins =
            Tuple.second newWins
    in
    List.foldl (updateDictFromWin copiesOfWinner) currWins wins


updateDictFromWin : Int -> Int -> Dict Int Int -> Dict Int Int
updateDictFromWin copiesOfWinner winner currWins =
    Dict.update winner (incrementCount copiesOfWinner) currWins


incrementCount : Int -> Maybe Int -> Maybe Int
incrementCount numCopies currVal =
    Maybe.map (\v -> v + numCopies) currVal



-- case currVal of
--     Just v ->
--         Just (v + numCopies)
--     Nothing ->
--         Just numCopies


cardOrDefault : Int -> Result (List DeadEnd) Card -> Card
cardOrDefault idx result =
    case result of
        Ok c ->
            c

        Err _ ->
            { id = idx + 1, winners = Set.empty, candidates = [] }


calculateObtainedCopies : Card -> List Int
calculateObtainedCopies card =
    let
        numWins =
            List.filter (\c -> Set.member c card.winners) card.candidates
                |> List.length
    in
    if numWins > 0 then
        List.range (card.id + 1) (card.id + numWins)

    else
        []


type alias Cards =
    Dict Int Card


type alias Card =
    { id : Int
    , winners : Set Int
    , candidates : List Int
    }


lineParser : Parser Card
lineParser =
    succeed Card
        |. keyword "Card"
        |. spaces
        |= int
        |. symbol ":"
        |. spaces
        |= loop [] winnersParser
        |. spaces
        |= loop [] candidatesParser


winnersParser : List Int -> Parser (Step (List Int) (Set Int))
winnersParser winners =
    oneOf
        [ succeed ()
            |. symbol "|"
            |> map (\_ -> Done (Set.fromList winners))
        , succeed (\winner -> Loop (winner :: winners))
            |= int
            |. spaces
        ]


candidatesParser : List Int -> Parser (Step (List Int) (List Int))
candidatesParser candidates =
    oneOf
        [ succeed ()
            |. end
            |> map (\_ -> Done (List.reverse candidates))
        , succeed (\candidate -> Loop (candidate :: candidates))
            |= int
            |. spaces
        ]



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ button [ onClick FileRequested ] [ text "Select File" ]
        , displayCalibrationValue model
        ]


displayCalibrationValue : Model -> Html Msg
displayCalibrationValue model =
    case model.output of
        Nothing ->
            div [] [ text "Select a text file to process" ]

        Just val ->
            div [] [ text (String.fromInt val) ]



-- Subscriptions


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none
