module Day3 exposing (..)

import Browser
import File exposing (File)
import File.Select as Select
import Html exposing (Html, button, div, p, text)
import Html.Events exposing (onClick)
import Parser exposing (..)
import String
import Task



-- MAIN


main : Program () Model Msg
main =
    Browser.element { init = init, update = update, view = view, subscriptions = subscriptions }



-- MODEL


type alias Model =
    { output : Maybe Int }


init : () -> ( Model, Cmd Msg )
init _ =
    ( { output = Nothing }, Cmd.none )



-- UPDATE


type Msg
    = FileRequested
    | FileLoaded File
    | TextRead String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        FileRequested ->
            ( model, selectFile )

        FileLoaded file ->
            ( model, readTextContents file )

        TextRead content ->
            ( { model | output = Just (calculateFileValue content) }, Cmd.none )


selectFile : Cmd Msg
selectFile =
    Select.file [ "text/plain" ] FileLoaded


readTextContents : File -> Cmd Msg
readTextContents file =
    Task.perform TextRead (File.toString file)


calculateFileValue : String -> Int
calculateFileValue content =
    let
        result =
            run fileParser content
    in
    case result of
        Ok components ->
            calculateValue components

        Err _ ->
            0


calculateValue : List Position -> Int
calculateValue components =
    let
        gears =
            List.filterMap getGear components

        parts =
            List.filterMap getPart components
    in
    List.map (gearRatio parts) gears
        |> List.foldl (+) 0


getGear : Position -> Maybe GearPosition
getGear component =
    case component of
        Gear g ->
            Just g

        _ ->
            Nothing


getPart : Position -> Maybe PartPosition
getPart component =
    case component of
        Part p ->
            Just p

        _ ->
            Nothing


type Position
    = Part PartPosition
    | Gear GearPosition


type alias PartPosition =
    { id : Int
    , row : Int
    , startCol : Int
    , endCol : Int
    }


type alias GearPosition =
    Location


type alias Location =
    ( Int, Int )


fileParser : Parser (List Position)
fileParser =
    succeed identity
        |. spacer
        |= loop [] positionsParser


positionsParser : List Position -> Parser (Step (List Position) (List Position))
positionsParser positions =
    oneOf
        [ succeed ()
            |. end
            |> map (\_ -> Done (List.reverse positions))
        , succeed (\comp -> Loop (comp :: positions))
            |= positionParser
            |. spacer
        ]


positionParser : Parser Position
positionParser =
    oneOf
        [ map (\pos -> Gear pos) gearPositionParser
        , map (\pos -> Part pos) partPositionParser
        ]


spacer : Parser ()
spacer =
    chompWhile isSpacer


isSpacer : Char -> Bool
isSpacer c =
    not (Char.isDigit c) && (c /= '*')


gearPositionParser : Parser GearPosition
gearPositionParser =
    getPosition
        |. chompIf isGear


isGear : Char -> Bool
isGear char =
    char == '*'


partPositionParser : Parser PartPosition
partPositionParser =
    succeed
        (\start value end ->
            { id = value
            , row = Tuple.first start
            , startCol = Tuple.second start
            , endCol = Tuple.second end - 1
            }
        )
        |= getPosition
        |= partIdParser
        |= getPosition


partIdParser : Parser Int
partIdParser =
    getChompedString (chompWhile Char.isDigit)
        |> andThen checkPartId


checkPartId : String -> Parser Int
checkPartId str =
    case String.toInt str of
        Just n ->
            succeed n

        Nothing ->
            problem "Not an int"


gearRatio : List PartPosition -> GearPosition -> Int
gearRatio parts gear =
    let
        adjLocs =
            adjacentLocations gear

        adjParts =
            List.filter (\part -> List.any (inLocation part) adjLocs) parts
    in
    case adjParts of
        a :: b :: [] ->
            a.id * b.id

        _ ->
            0


adjacentLocations : GearPosition -> List Location
adjacentLocations gear =
    let
        x =
            Tuple.first gear

        y =
            Tuple.second gear

        upperRows =
            List.range (y - 1) (y + 1)
                |> List.map (\col -> ( x - 1, col ))

        middleRows =
            [ ( x, y - 1 )
            , ( x, y + 1 )
            ]

        lowerRows =
            List.range (y - 1) (y + 1)
                |> List.map (\col -> ( x + 1, col ))
    in
    upperRows ++ middleRows ++ lowerRows


inLocation : PartPosition -> Location -> Bool
inLocation part loc =
    let
        row =
            Tuple.first loc

        col =
            Tuple.second loc
    in
    part.row == row && List.member col (List.range part.startCol part.endCol)



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ button [ onClick FileRequested ] [ text "Select File" ]
        , displayCalibrationValue model
        ]


displayCalibrationValue : Model -> Html Msg
displayCalibrationValue model =
    case model.output of
        Nothing ->
            div [] [ text "Select a text file to process" ]

        Just val ->
            div [] [ text (String.fromInt val) ]



-- Subscriptions


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none
