module Day1 exposing (..)

import Browser
import File exposing (File)
import File.Select as Select
import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)
import String
import Task



-- MAIN


main : Program () Model Msg
main =
    Browser.element { init = init, update = update, view = view, subscriptions = subscriptions }



-- MODEL


type alias Model =
    { calibration : Maybe Int }


init : () -> ( Model, Cmd Msg )
init _ =
    ( { calibration = Nothing }, Cmd.none )



-- UPDATE


type Msg
    = FileRequested
    | FileLoaded File
    | TextRead String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        FileRequested ->
            ( model, selectCalibrationFile )

        FileLoaded file ->
            ( model, readCalibrationDocument file )

        TextRead content ->
            ( { model | calibration = calculateFileValue content }, Cmd.none )


selectCalibrationFile : Cmd Msg
selectCalibrationFile =
    Select.file [ "text/plain" ] FileLoaded


readCalibrationDocument : File -> Cmd Msg
readCalibrationDocument file =
    Task.perform TextRead (File.toString file)


calculateFileValue : String -> Maybe Int
calculateFileValue content =
    Just (List.foldl (+) 0 (content |> String.lines |> List.map calculateLineValue))


calculateLineValue : String -> Int
calculateLineValue line =
    let
        strValue =
            String.join "" [ findFirstDigit line, findLastDigit line ]
    in
    if String.isEmpty strValue then
        0

    else
        Maybe.withDefault 0 (String.toInt strValue)


findFirstDigit : String -> String
findFirstDigit str =
    case getDigit String.startsWith str of
        Just digit ->
            digit

        Nothing ->
            case String.uncons str of
                Nothing ->
                    ""

                Just ( ch, rest ) ->
                    if Char.isDigit ch then
                        String.fromChar ch

                    else
                        findFirstDigit rest


findLastDigit : String -> String
findLastDigit str =
    case getDigit String.endsWith str of
        Just digit ->
            digit

        Nothing ->
            case String.uncons (String.reverse str) of
                Nothing ->
                    ""

                Just ( ch, rest ) ->
                    if Char.isDigit ch then
                        String.fromChar ch

                    else
                        findLastDigit (String.reverse rest)


getDigit : (String -> String -> Bool) -> String -> Maybe String
getDigit chk str =
    if chk "one" str then
        Just "1"

    else if chk "two" str then
        Just "2"

    else if chk "three" str then
        Just "3"

    else if chk "four" str then
        Just "4"

    else if chk "five" str then
        Just "5"

    else if chk "six" str then
        Just "6"

    else if chk "seven" str then
        Just "7"

    else if chk "eight" str then
        Just "8"

    else if chk "nine" str then
        Just "9"

    else
        Nothing



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ button [ onClick FileRequested ] [ text "Select File" ]
        , displayCalibrationValue model
        ]


displayCalibrationValue : Model -> Html Msg
displayCalibrationValue model =
    case model.calibration of
        Nothing ->
            div [] [ text "Select a text file to process" ]

        Just val ->
            div [] [ text (String.fromInt val) ]



-- Subscriptions


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none
