module Day2 exposing (..)

import Browser
import File exposing (File)
import File.Select as Select
import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)
import Parser exposing (..)
import String
import Task



-- MAIN


main : Program () Model Msg
main =
    Browser.element { init = init, update = update, view = view, subscriptions = subscriptions }



-- MODEL


type alias Model =
    { sumGameIds : Maybe Int }


init : () -> ( Model, Cmd Msg )
init _ =
    ( { sumGameIds = Nothing }, Cmd.none )



-- UPDATE


type Msg
    = FileRequested
    | FileLoaded File
    | TextRead String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        FileRequested ->
            ( model, selectFile )

        FileLoaded file ->
            ( model, readTextContents file )

        TextRead content ->
            ( { model | sumGameIds = calculateFileValue content }, Cmd.none )


selectFile : Cmd Msg
selectFile =
    Select.file [ "text/plain" ] FileLoaded


readTextContents : File -> Cmd Msg
readTextContents file =
    Task.perform TextRead (File.toString file)


calculateFileValue : String -> Maybe Int
calculateFileValue content =
    Just (List.foldl (+) 0 (content |> String.lines |> List.map calculateLineValue))


calculateLineValue : String -> Int
calculateLineValue line =
    let
        parsedLine =
            run lineParser line
    in
    case parsedLine of
        Ok game ->
            getMaxOfColour Red game.reveals * getMaxOfColour Green game.reveals * getMaxOfColour Blue game.reveals

        Err _ ->
            0


getMaxOfColour : Colour -> List Reveal -> Int
getMaxOfColour colour reveals =
    let
        colorCounts =
            List.map (getColourCount colour) reveals
                |> List.filter (\maybeCount -> maybeCount /= Nothing)
                |> List.map (\maybeCount -> Maybe.withDefault 0 maybeCount)
    in
    List.maximum colorCounts
        |> Maybe.withDefault 0


getColourCount : Colour -> Reveal -> Maybe Int
getColourCount colour reveal =
    reveal
        |> List.filter (\colourCount -> Tuple.first colourCount == colour)
        |> List.map Tuple.second
        |> List.head


type alias Game =
    { id : Int
    , reveals : List Reveal
    }


type alias Reveal =
    List ColourCount


type alias ColourCount =
    ( Colour, Int )


type Colour
    = Red
    | Green
    | Blue


lineParser : Parser Game
lineParser =
    succeed Game
        |. keyword "Game"
        |. spaces
        |= int
        |. symbol ":"
        |. spaces
        |= loop [] revealParser


revealParser : List Reveal -> Parser (Step (List Reveal) (List Reveal))
revealParser reveals =
    oneOf
        [ succeed ()
            |. end
            |> map (\_ -> Done (List.reverse reveals))
        , succeed (\reveal -> Loop (reveal :: reveals))
            |= loop [] colourCountsParser
        ]


colourCountsParser : List ColourCount -> Parser (Step (List ColourCount) Reveal)
colourCountsParser colourCounts =
    oneOf
        [ succeed (\colourCount -> Loop (colourCount :: colourCounts))
            |. symbol ","
            |. spaces
            |= colourCountParser
        , succeed (\colourCount -> Loop (colourCount :: colourCounts))
            |. spaces
            |= colourCountParser
        , succeed ()
            |. endOfColourCountsParser
            |> map (\_ -> Done (List.reverse colourCounts))
        ]


endOfColourCountsParser : Parser ()
endOfColourCountsParser =
    oneOf
        [ succeed ()
            |. symbol ";"
        , succeed ()
            |. end
        ]


colourCountParser : Parser ColourCount
colourCountParser =
    succeed Tuple.pair
        |= int
        |. spaces
        |= colourParser
        |> andThen (\( count, colour ) -> succeed ( colour, count ))


colourParser : Parser Colour
colourParser =
    Parser.oneOf
        [ map (\_ -> Red) (token "red")
        , map (\_ -> Green) (token "green")
        , map (\_ -> Blue) (token "blue")
        ]



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ button [ onClick FileRequested ] [ text "Select File" ]
        , displayCalibrationValue model
        ]


displayCalibrationValue : Model -> Html Msg
displayCalibrationValue model =
    case model.sumGameIds of
        Nothing ->
            div [] [ text "Select a text file to process" ]

        Just val ->
            div [] [ text (String.fromInt val) ]



-- Subscriptions


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none
